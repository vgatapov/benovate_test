from django.test import TestCase
from django.contrib.auth.models import User


class TransactionTest(TestCase):
    def setUp(self) -> None:
        inn_list = ["0000000000", "0123456789", "9876543210"]
        for i in inn_list:
            user = User.objects.create(username=f'user_{i}', password='12test12', email=f'test_{i}@example.com')
            user.save()
            user.userfields.inn = i
            user.userfields.balance = 100
            user.userfields.save()

    def test_transaction(self):
        data = {"sender_inn": "0000000000", "quantity": 7.00, "receiver_inn_list": ["0123456789", "9876543210"]}
        response = self.client.post('/api/transaction/', data)
        self.assertEquals(response.status_code, 200)

        data['sender_inn'] = "0000000001"
        response = self.client.post('/api/transaction/', data)
        self.assertEquals(response.status_code, 404)

        data['receiver_inn_list'] = []
        response = self.client.post('/api/transaction/', data)
        self.assertEquals(response.status_code, 400)

        data['quantity'] = 0
        response = self.client.post('/api/transaction/', data)
        self.assertEquals(response.status_code, 400)
