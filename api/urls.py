from django.urls import path
from .views import CreateTransaction


urlpatterns = [
    path('transaction/', CreateTransaction.as_view())
]

