from .models import Transaction
from django.contrib.auth.models import User
from decimal import Decimal
from typing import Dict
from rest_framework.exceptions import NotFound, ParseError
from django.db import transaction


class TransactionService:
    def __init__(self, serializer_data: Dict):
        self.serializer_data = serializer_data
        self.response = {'status': False}

    def __check_balance(self, user: User, quantity: Decimal) -> bool:
        if user.userfields.balance > quantity:
            return True
        raise ParseError(detail='Не достаточно средств', code=400)

    def __get_user(self, inn: str) -> User:
        user = User.objects.filter(userfields__inn=inn).first()
        if user:
            return user
        raise NotFound(detail=f'Пользователь с ИНН {inn} не найден', code=404)

    def make_transaction(self):
        user_from = self.__get_user(self.serializer_data.get('sender_inn'))
        quantity = self.serializer_data.get('quantity')
        users_to = self.serializer_data.get('receiver_inn_list')
        quantity_for_user = quantity / len(users_to)

        users_to_list = []
        for user_id in users_to:
            users_to_list.append(self.__get_user(user_id))

        def set_transaction_status(tr: Transaction) -> None:
            tr.status = True
            tr.save()

        if self.__check_balance(user_from, quantity):
            with transaction.atomic():
                user_from.userfields.balance -= quantity
                user_from.userfields.save()
                for user_to in users_to_list:
                    tr = Transaction(user_from=user_from, user_to=user_to, quantity=quantity_for_user)
                    tr.save()
                    user_to.userfields.balance += quantity_for_user
                    user_to.userfields.save()

                transaction.on_commit(lambda: set_transaction_status(tr))
                self.response['status'] = True

