from rest_framework.views import APIView
from rest_framework import status
from rest_framework.request import Request
from rest_framework.response import Response
from .serializers import TransactionSerializer
from .services import TransactionService


class CreateTransaction(APIView):
    def post(self, request: Request) -> Response:
        serializer = TransactionSerializer(data=request.data)
        if serializer.is_valid():
            service = TransactionService(serializer.validated_data)
            service.make_transaction()
            return Response(service.response, status=status.HTTP_200_OK)
        else:
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)




