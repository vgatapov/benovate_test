from django.db import models
from django.contrib.auth.models import User
from decimal import Decimal
from django.db.models.signals import post_save
from django.dispatch import receiver


class UserFields(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    inn = models.CharField(max_length=10, unique=True)
    balance = models.DecimalField(max_digits=19, decimal_places=2, default=Decimal('0.00'))

    objects = models.Manager()


class Transaction(models.Model):
    user_from = models.ForeignKey(User, on_delete=models.DO_NOTHING, related_name='transaction_user_from')
    user_to = models.ForeignKey(User, on_delete=models.DO_NOTHING, related_name='transaction_user_to')
    quantity = models.DecimalField(max_digits=19, decimal_places=2, default=Decimal('0.00'))
    status = models.BooleanField(default=False)

    objects = models.Manager()


@receiver(post_save, sender=User)
def create_user_fields(sender, instance, created, **kwargs):
    if created:
        UserFields.objects.create(user=instance)


@receiver(post_save, sender=User)
def save_user_fields(sender, instance, **kwargs):
    instance.userfields.save()

