from rest_framework import serializers
from decimal import Decimal
from typing import Dict, Union


class TransactionSerializer(serializers.Serializer):
    sender_inn = serializers.CharField()
    quantity = serializers.DecimalField(max_digits=19, decimal_places=2)
    receiver_inn_list = serializers.ListField(child=serializers.CharField())

    def validate(self, data: Dict) -> Dict:
        sender_inn = data.get('sender_inn')
        quantity = Decimal(data.get('quantity'))
        receiver_inn_list = data.get('receiver_inn_list')
        if quantity == 0:
            raise serializers.ValidationError(detail="Количество не должно быть 0")
        if quantity < 0:
            raise serializers.ValidationError(detail="Количество должно быть больше 0")

        if len(receiver_inn_list) < 1:
            raise serializers.ValidationError(detail="Нет получателей")

        for inn in receiver_inn_list:
            if inn == sender_inn:
                raise serializers.ValidationError(detail="Среди получателей не должно быть отправителя")

        return data

