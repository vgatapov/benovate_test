from django.contrib import admin
from .models import Transaction, UserFields


admin.site.register(Transaction)
admin.site.register(UserFields)
