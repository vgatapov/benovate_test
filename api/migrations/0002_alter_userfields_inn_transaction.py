# Generated by Django 4.1.5 on 2023-01-26 15:19

from decimal import Decimal
from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('api', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='userfields',
            name='inn',
            field=models.CharField(max_length=10, unique=True),
        ),
        migrations.CreateModel(
            name='Transaction',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('quantity', models.DecimalField(decimal_places=2, default=Decimal('0.00'), max_digits=19)),
                ('user_from', models.ForeignKey(on_delete=django.db.models.deletion.DO_NOTHING, related_name='transaction_user_from', to=settings.AUTH_USER_MODEL)),
                ('user_to', models.ForeignKey(on_delete=django.db.models.deletion.DO_NOTHING, related_name='transaction_user_to', to=settings.AUTH_USER_MODEL)),
            ],
        ),
    ]
